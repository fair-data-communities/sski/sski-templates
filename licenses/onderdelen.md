# Onderdelen

## Templates
1. Voor het implementeren van de data aanbiedende kant worden de onderdelen gebruikt die hieronder bij 2 benoemd worden.
2. De volgende templates zijn nodig:
  1. SSKI Data Plan Template. Deze kan gevonden worden op https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/dataplan_templates/dataplan_template.json
  2. SSKI Metadata Templates. Deze kunnen gevonden worden op https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/definitions/assets.rdf
3. De aangeboden assets data worden gecheckt op bruikbaarheid middels de SSKI FAIR-Data Evaluator assets.

### FAIR-Score Assets 
1. De aangeboden assets data worden gecheckt op bruikbaarheid middels de SSKI
FAIR-Data Evaluator assets
2. De FAIR-score van deze evaluatie moet minstens XX% zijn, zodat de rest van de
community de data goed kan interpreteren.
3. De FAIR-score moet erkend worden door middel van een GO FAIR Foundation
certificaat.

### SSKI FAIR-Data Evaluators
1. De SSKI FAIR-Data Evaluator wordt jaarlijks vernieuwd.
2. Deelnemers moeten tenminste voldoen aan huidige versie – 2 jaar.
