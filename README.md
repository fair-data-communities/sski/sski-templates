# FAIR-Data Community Samenwerken in de Ondergrond (SSKI)

## Officiele repository
Officiele repository voor de data plan template, metadata template, FAIR-evalautor van de SSKI FAIR-Data community is https://gitlab.com/fair-data-communities/sski/sski-templates

## Omschrijving
Deze repository bevat de open source assets voor data aanbieders van de FAIR data community 'SSKI' (Samenwerken in de ondegrond).De oorspronkelijke locatie van deze repository is https://gitlab.com/fair-data-communities/sski/sski-templates

## SSKI Data plan template Assets
Het data plan template is gemaakt in JSON. De template bevat de SSKI Community specifieke data plan vragen omtrent assets. Je kan deze file gebruiken als seeding file en als databron. Locatie: https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/dataplan_templates/assets/dataplan_template.json

## SSKI Data plan template Planningen
Het data plan template is gemaakt in JSON. De template bevat de SSKI Community specifieke data plan vragen omtrent planningen. Je kan deze file gebruiken als seeding file en als databron. Locatie: https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/dataplan_templates/planning/dataplan_template.json


## SSKI Metadata dataplan templates
De metadata templates zijn in RDF aangeleverd. Locatie: https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/definitions/sski.rdf

## SSKI FAIR-Data Evaluator Assets
De SSKI evaluatoren zijn in Ruby aangeleverd. Locatie: https://gitlab.com/fair-data-communities/sski/sski-evaluator

## Open Source Licentie
De licentie wordt op volgende locatie beschreven: https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/LICENSE

## Gebruikerslicentie
Alle data-aanbieders en datagebruikers binnen de SSKI FAIR-Data Community gaan met de volgende gebruikerslicentie akkoord: https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/licenses/user_license.md

## Community Beheer
Het community beheer van deze open source assets, geintroduceerd voor de FAIR-Data Community Samenwerken in de Ondergrond (SSKI), wordt gedaan door de Purple Polar Bear B.V.

## Contributie
Zie de contributing file: https://gitlab.com/fair-data-communities/sski/sski-templates/-/blob/main/CONTRIBUTING

## Versies
De officiele versies worden gekenmerkt door tags die het formaat hebben van: release_jaartal. Daarnaast bevat master altijd de laatste gereleasde versie. Doorlopende ontwikkelingen vinden plaats in de development branch.
